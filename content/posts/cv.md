---
layout: post
title: Resume/CV
subtitle: For your software needs
description: Quinn Dougherty's CV for software, data science, proof engineering, IT projects
slug: cv
---
# Quinn Dougherty - quinnd@riseup.net
[github](https://github.com/quinn-dougherty) - [gitlab](https://gitlab.com/quinn-dougherty) - [linkedin](https://linkedin.com/in/quinn-dougherty)
## Keywords
python3 - linux - cloud - CI/CD - AWS - javascript - coq - rescript - docker - SQL - supervised learning - unsupervised learning - reinforcement learning - testing - pandas - numpy - tensorflow - pytorch - bash - ray (rllib) - REST APIs - object-oriented programming - functional programming - latex - decentralized finance (defi)

## Work
### Quantified Uncertainty Research Institute
- [`squiggle`](https://github.com/quantified-uncertainty/squiggle) ([view PRs](https://github.com/quantified-uncertainty/squiggle/pulls?q=is%3Apr+author%3Aquinn-dougherty))
- Language design and engineering
- Rescript
- Developer experience design (team size: four) 
- Property-based testing on numeric code
- March 2022 - Present
### Platonic.Systems
#### Ardana
- Auditing novel decentralized finance protocols
- Formal verification
- Haskell ecosystem: Cabal, Nix
- Research
- August - December 2021
#### Orbis
- TinyRAM emulator in Coq
- Zero-knowledge proofs
- Research
- January - March 2022
### Stanford Existential Risks Initiative
- Research
- June - August 2021
### Sandbox Banking
- Maintaining, adding features, and testing a JavaScript (Vue) frontend
- Maintaining, adding features, and testing a Python (Django) backend
- Implementing CI/CD including pushing a container to AWS Elastic Container Registry via GitHub Action
- Linux system administration in AWS servers
- April - December 2020

## Open Source
### Cooperative AI in Common Pool Resource Problems
- [View code](https://github.com/RedTachyon/cpr_reputation/pulls?q=is%3Apr+author%3Aquinn-dougherty)
- Gridworld development (NumPy)
- Multi-agent reinforcement learning (PPO with Ray and RLLib) 
- Performing literature review
- February - May 2021
### Covid-19 Hospital Impact Model for Epidemics (CHIME) 
- [View code](https://github.com/CodeForPhilly/chime/pulls?q=is%3Apr+author%3Aquinn-dougherty)
- Taking mission-critical mathematical/epidemiological content from notebook to production
- Code review and project management on a team of more than 20 contributors
- March 2020 

## Certification
### Triplebyte Machine Learning Engineer
- [view cert](https://triplebyte.com/tb/quinn-dougherty-kd5mtkq/certificate/track/ml)
- Top 3% in rigorous two-round interview
- Examination included databases, data structures & algorithms, system architecture, machine learning, data science
- January 2020 - indefinitely
### TensorFlow Developer
- [view cert](https://gitlab.com/quinn-dougherty/quinnd-blog/-/blob/main/static/tensorflow-cert.pdf)
- Examination included computer vision, time series, natural language processing, training deep learning models
- June 2020 - June 2023

## Selected Writing & Publications - [view Semantic Scholar](https://www.semanticscholar.org/author/Quinn-Dougherty/41156871) - [view Google Scholar](https://scholar.google.com/citations?hl=en&user=IyBRXeAAAAAJ)
- Bevensee, E., Aliapoulios, M., Dougherty, Q., Baumgartner, J., McCoy, D., & Blackburn, J. (2020). SMAT: The Social Media Analysis Toolkit. ICWSM. https://doi.org/10.36190/2020.18
- Wang, P.; Liu, K.; Dougherty, Q. Conceptions of Artificial Intelligence and Singularity. Information 2018, 9, 79
- [High Impact Careers in Formal Verification](https://forum.effectivealtruism.org/posts/4rMxiyPTPdzaFMyGm/high-impact-careers-in-formal-verification-artificial) (Effective Altruism Forum)

## Effective Altruism
- Center for Enabling Effective Altruist Learning and Research - grant recipient (six months, 2021)
- EA Oxford In-Depth Fellowship - participant (spring 2021)
- EA Cambridge AGI Safety Fellowship - facilitator (summer 2021)
- EA Philadelphia - MC several events, coorganizer (2020-present)
- [Writing](https://forum.effectivealtruism.org/users/quinn)
- EAGxBoston 2022 - talk "A Simple Theory of Goal Refinement" 

## Education
### Bloom Institute of Technology (_formally known as Lambda School_) 
- Statistics, data science, software engineering
- Time there included working as a manager of teaching assistants, which included working as a teaching assistant
- Graduated June 2019, work finished October 2019
### Community College of Philadelphia
- Calculus, linear algebra, statistics, differential equations, discrete math
- GPA: 3.9
- Time there included working as a tutor for the whole math department
- Classes taken 2016-2018, work ongoing

## Additional Experience
### Swine Films
- Operations
- Logistics
- Schedules & budgets
- Music
- 2010-2017 (off and on)
