// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
  siteName: 'quinnd',
  siteDescription: 'programmer',
  icon: "src/favicon.png",
  templates: {
    Post: '/blog/:slug'
  },
  transformers: {
    remark: {
      plugins: ['remark-math', 'remark-html-katex', '@gridsome/remark-prismjs']
    }
  },
  plugins: [
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'content/posts/**/*.md',
        typeName: 'Post',
      }
    }
  ]
}
